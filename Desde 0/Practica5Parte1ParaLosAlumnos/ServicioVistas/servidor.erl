%% AUTOR: Daniel Cabrera, Guillermo Cebollero
%% NIA:686013, 647782
%% FICHERO: servidor.erl
%% TIEMPO: 20h
%% DESCRIPCI’ON: Servidor de vistas tolerante a fallos
%% ----------------------------------------------------------------------------
%% servidor : Modulo servidor de vistas
%%
%% ----------------------------------------------------------------------------

-module(servidor).
-include("sv.hrl").
-include_lib("eunit/include/eunit.hrl").

-export([start/2, stop/1]).

-export([init_sv/0, init_monitor/0]).



 %% Registro que guarda el estado del servidor de vistas
 -record(estado_sv, {vista :: vista,pings :: [node()]}).



%%%%%%%%%%%%%%%%%%%% Interface (FUNCIONES EXPORTABLES)  %%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Poner en marcha el servicio de vistas con 2 procesos concurrentes
-spec start( atom(), atom() ) -> node().
start(Host, NombreNodo) ->
   % ?debugFmt("Arrancar un nodo servidor vistas~n",[]),

    %%%%% VUESTRO CODIGO DE INICIALIZACION
    
     % args para comando remoto erl
     Args = "-connect_all false -setcookie palabrasecreta",
        % arranca servidor en nodo remoto
        {ok, Nodo} = slave:start(Host, NombreNodo, Args),
  %  ?debugFmt("Nodo servidor vistas en marcha : ~p~n",[Nodo]),
  process_flag(trap_exit, true),
  spawn_link(Nodo, ?MODULE, init_sv, []),
  Nodo.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parar nodo servidor de vista al completo, includos los 2 procesos
-spec stop( atom() ) -> ok.
stop(Nodo) ->
slave:stop(Nodo),
timer:sleep(10),
comun:vaciar_buzon(),
ok.



%%------------------------  FUNCIONES LOCALES  ------------------------------%%


%%-----------------------------------------------------------------------------
init_sv() ->
register(sv, self()),
spawn_link(?MODULE, init_monitor, []),

SV = #estado_sv{vista=vista:vista_inicial(), pings = []},


bucle_recepcion(SV).


%%-----------------------------------------------------------------------------
bucle_recepcion(SV) ->
receive
	{ping, NodoOrigen, NumVista} ->

	if
		%Si NumVista es 0 significa que es la primera vez que llega 
		%o ha rearrancado
		NumVista =:= 0	->
		NumVistaServidor = vista:num_vista(SV#estado_sv.vista),
			%Si el numero de vista en el servidor es 0 
			%poner como primario y enviar al cliente
			if
				NumVistaServidor =:= 0	->
				SV2 = SV#estado_sv{vista 
				= vista:nueva_vista(NumVistaServidor + 1,
					NodoOrigen, undefined)},
				{cliente, NodoOrigen} ! {vista_tentativa, 
				SV2#estado_sv.vista, true};
				
				NumVistaServidor =:= 1	->
				   %Si existe un primario, y no hay copia y 
				   %vista en el servidor es 1, poner como copia
				   IdPrimario = vista:primario(SV#estado_sv.vista),
				   SV2 = SV#estado_sv{vista = 
				   vista:nueva_vista(NumVistaServidor + 1, IdPrimario,
				   	NodoOrigen)},
				   {cliente, NodoOrigen} ! {vista_tentativa,
				   SV2#estado_sv.vista, true};
				   true	->
					%Existe primario y copia, añadir a la lista 
					%de pings devolver vista actual
					SV2 = SV#estado_sv{pings = lists:append(
						SV#estado_sv.pings,[NodoOrigen])},
					{cliente, NodoOrigen} ! {vista_tentativa,
					SV2#estado_sv.vista, true}
				end
				;  
				true	->
               %?debugFmt("ping~n",[]),
			%Añadir a la lista de pings devolver vista actual
			SV2 = SV#estado_sv{pings 
			= lists:append(SV#estado_sv.pings,[NodoOrigen])},
			{cliente, NodoOrigen} ! {vista_tentativa,SV2#estado_sv.vista,true}
		end
		,
		bucle_recepcion(SV2);
		{obten_vista, Pid} ->
           %Devolver vista actual
           Pid ! {vista_valida, SV#estado_sv.vista},
           bucle_recepcion(SV);
           
           procesa_situacion_servidores ->
           procesar_situacion_servidores(SV)
           
       end.
%%-----------------------------------------------------------------------------
init_monitor() ->
sv ! procesa_situacion_servidores,
timer:sleep(?INTERVALO_PING),
init_monitor().

%%----------------------------------------------------------------------------- 
procesar_situacion_servidores(SV) ->
    %revisar que primario y copia siguen vivos
    %en caso de no estar vivo el primario, pasar copia a primario y 
    %poner un nuevo copia(si existe)
    %en caso de no estar vivo el secundario, poner un nuevo copia(si existe)
    %vaciar lista de pines
    %?debugFmt("lista pings: ~p~n",[SV#estado_sv.pings]),
    if
    	length(SV#estado_sv.pings) =:= 0 ->
    	Primario_vivo = false,
    	Copia_vivo = false;
    	true ->
    	Primario_vivo 
    	= lists:member(vista:primario(SV#estado_sv.vista),SV#estado_sv.pings),
    	Copia_vivo 
    	= lists:member(vista:copia(SV#estado_sv.vista),SV#estado_sv.pings)
    end,
    Pings = deleteall(vista:primario(SV#estado_sv.vista),SV#estado_sv.pings),
    PingList = deleteall(vista:copia(SV#estado_sv.vista),Pings), 
    %%Lista de pings sin primario y copia (si están vivos)
    case {Primario_vivo,Copia_vivo} of
    	{false,false} ->
    	NumVista = vista:num_vista(SV#estado_sv.vista),
    	if NumVista > 0 ->
    		?debugFmt(">> WARNING: primario y copia muertos~n",[]),
    		Nueva_vista = vista:nueva_vista(
    			vista:num_vista(SV#estado_sv.vista),undefined,undefined),
    		SV2 = SV#estado_sv{vista=Nueva_vista,pings=[]},
    		bucle_recepcion(SV2);
    		true    ->
    		SV2 = SV#estado_sv{pings=[]},
    		bucle_recepcion(SV2)
    	end;

    	{false,true} ->
    	?debugFmt(">> WARNING: primario muerto~n",[]),
    	if
    		length(PingList)>0 ->
    		?debugFmt(">> Copia a pasado a primario, ultimo
    			nodo en hacer ping pasado a copia~n",[]),
    		Nueva_vista = vista:nueva_vista(
    			vista:num_vista(SV#estado_sv.vista)+1,vista:copia(
    				SV#estado_sv.vista),lists:last(PingList)),
    		SV2 = SV#estado_sv{vista=Nueva_vista,pings=[]};
                true    -> %%No hay pings para poner a copia
                ?debugFmt(">> Copia a pasado a primario~n",[]),
                Nueva_vista = vista:nueva_vista(vista:num_vista(
                	SV#estado_sv.vista)+1,vista:copia(
                	SV#estado_sv.vista),undefined),
                SV2 = SV#estado_sv{vista=Nueva_vista,pings=[]}
            end,
            bucle_recepcion(SV2);
            {true,false} ->
            ?debugFmt(">> WARNING: copia muerto~n",[]),
            if
                length(PingList)>0 -> %% Tenemos para reponer
                ?debugFmt(">> Ultimo nodo en hacer ping pasado a copia~n",[]),
                Nueva_vista = vista:nueva_vista(vista:num_vista(
                	SV#estado_sv.vista)+1,vista:primario(
                	SV#estado_sv.vista),lists:last(PingList)),
                SV2 = SV#estado_sv{vista=Nueva_vista,pings=[]};
                true    ->
                ?debugFmt(">> No hay para reponer copia~n",[]),
                SV2 = SV#estado_sv{pings=[]}
            end, 
            bucle_recepcion(SV2);  
            {true,true} ->
            %?debugFmt(">> primario y copia vivos~n",[]),
            %Vaciamos la lista de pings
            SV2 = SV#estado_sv{pings=[]},
            bucle_recepcion(SV2)
        end.
        deleteall(Elem, List) ->
        [I || I <- List, I =/= Elem].