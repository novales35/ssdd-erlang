%% ----------------------------------------------------------------------------
%% servidor : Modulo servidor de vistas
%%
%% ----------------------------------------------------------------------------

-module(servidor).
-include("sv.hrl").
-include_lib("eunit/include/eunit.hrl").

-export([start/2, stop/1]).

-export([init_sv/0, init_monitor/0]).



 %% Registro que guarda el estado del servidor de vistas
 %% COPIO Y PEGO DEL PDF
 %% El gestor de vistas gestionará una secuencia de "vistas" numeradas,
 %% cada una con el trio {nº de vista, identificador nodo primario,
 %% identificador nodo copia}; válido para cada vista

-record(estado_sv,{vista :: vista,
                   clientes :: [node()],
                   pings :: [node()]

    }).


%%%%%%%%%%%%%%%%%%%% Interface (FUNCIONES EXPORTABLES)  %%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Poner en marcha el servicio de vistas con 2 procesos concurrentes
-spec start( atom(), atom() ) -> node().
start(Host, NombreNodo) ->
    ?debugFmt("Arrancar un nodo servidor vistas~n",[]),
    %%%%% VUESTRO CODIGO DE INICIALIZACION
     % args para comando remoto erl
    Args = "-connect_all false -setcookie palabrasecreta",
        % arranca servidor en nodo remoto
    {ok, Nodo} = slave:start(Host, NombreNodo, Args),
  %  ?debugFmt("Nodo servidor vistas en marcha : ~p~n",[Nodo]),
    process_flag(trap_exit, true),
    spawn_link(Nodo, ?MODULE, init_sv, []),
    ?debugFmt("Nodo servidor vistas iniciado~n",[]),
    Nodo.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parar nodo servidor de vista al completo, includos los 2 procesos
-spec stop( atom() ) -> ok.
stop(Nodo) ->
    slave:stop(Nodo),
    timer:sleep(10),
    comun:vaciar_buzon(),
    ok.
    


%%------------------------  FUNCIONES LOCALES  ------------------------------%%


%%-----------------------------------------------------------------------------
init_sv() ->
    register(sv, self()),
    spawn_link(?MODULE, init_monitor, []),
    %%%%% VUESTRO CODIGO DE INICIALIZACION AQUI
    %%inicializamos el record
    SV = #estado_sv{vista=vista:vista_inicial(),clientes = [], pings = [] },
    bucle_recepcion(SV).
    
    
%%-----------------------------------------------------------------------------
bucle_recepcion(SV) ->
    io:fwrite("entro en bucle_recepcion\n"),
    receive
        {ping, NodoOrigen, NumVista} ->
            io:fwrite("match ping\n"),
            %revisar si la vista esta bien(y si es mayor?)
            if 
                %Si la vista es 0, significa que NodoOrigen se ha caido
                NumVista == 0   ->
                    io:fwrite("recibida vista = 0\n"),
                    PingList= SV#estado_sv.pings ++ NodoOrigen,
                    SV2 = SV#estado_sv{pings=PingList},
                    HaSidoEncontrado = true,
                    Vista = SV2#estado_sv.vista,
                    Vista2 = vista:nueva_vista(1,NodoOrigen,undefined),
                    {cliente, NodoOrigen} ! {vista_tentativa, Vista, true},
                    bucle_recepcion(SV2);
                true            ->
                    %% NumVista!=0 -> no se ha caido, es ping
                    SV#estado_sv.pings ++ NodoOrigen
            end
            %responder
            ;
        {obten_vista, Pid} ->
            ?debugFmt("MATCH obten_vista~n",[]),
            %reenviar vista
            Pid ! {vista_valida,SV#estado_sv.vista};
       
        procesa_situacion_servidores ->
            io:fwrite("match procesa_situacion_servidores\n"),
            procesar_situacion_servidores(SV)
            %%%%% VUESTRO CODIGO
    end,
    bucle_recepcion(SV).
    

%%-----------------------------------------------------------------------------
init_monitor() ->
    sv ! procesa_situacion_servidores,
    timer:sleep(?INTERVALO_PING),
    init_monitor().
 
%%----------------------------------------------------------------------------- 
procesar_situacion_servidores(SV) ->
    %%No sabemos que clientes están vivos
    SV2=SV#estado_sv{clientes=[]},
    %%Miramos quien está vivo
    procesar_pings(SV2).
                                %%%%% VUESTRO CODIGO

procesar_pings(SV) ->
%% vamos a guardar solo los clientes vivos
io:fwrite("entro en procesar pings\n"),
    PingList = SV#estado_sv.pings,
    case SV#estado_sv.pings of
        [H|T]   ->  SV2=SV#estado_sv{pings=T}, %%Lo eliminamos
                    case lists:member(H,SV#estado_sv.clientes) of
                        true ->
                            SV3=SV2,
                            ok; %%Ya estaba
                        false -> %else
                            Clientes_nuevo = SV2#estado_sv.clientes ++ H,
                            SV3=SV2#estado_sv{clientes=Clientes_nuevo},
                            SV#estado_sv.clientes ++ H
                            %% lo insertados
                    end,
                    io:fwrite("un ping procesado\n"),
                    procesar_pings(SV3);
        []      ->  ok, %% todos los pings procesados
                    io:fwrite("terminado\n")
    end.



